import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {NgFor, NgIf} from '@angular/common';
import {MatListModule} from '@angular/material/list'
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClient } from '@angular/common/http';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import youtubeChannels from './youtube.json';
import profiles from './facebook.json';
import profilesOrdinary from './facebookOrdinary.json';
import sites from './sites.json';
import telegramChannels from './telegram.json';
import twitterChannels from './twitter.json';

declare const window: any;

@Component({
  selector: 'select-channel',
  templateUrl: 'select-channel.component.html',
  styleUrls: ['select-channel.component.css'],
  standalone: true,
  imports: [MatFormFieldModule, MatListModule, MatSelectModule, NgFor, NgIf, MatInputModule, FormsModule, CdkAccordionModule],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SelectChannel {
  youtubeChannels: any[] = [];
  facebookFamous: any[] = [];
  facebookOrdinary: any[] = [];
  sites: any[] = [];
  telegramChannels: any[] = [];
  twitterChannels: any[] = [];
  creator: string = "";

  items = [
    'Youtube Канали',
    'Facebook Профили I част',
    'Facebook Профили II част',
    'Telegram Канали',
    'Страници',
    'Twitter Канали'
  ];
  expandedIndex = 1;
  isMobile = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };

  constructor(private http: HttpClient){
    //youtubeChannels.youtubeChannels.map( m=> this.latestYoutubeClip(m.channelId, m.name));
    youtubeChannels.youtubeChannels.map( m=> this.loadYoutubeChannels(m.link, m.name));
    profiles.profiles.filter(m =>m.enabled !== false).map( m=> this.loadProfiles(m.link, m.name, m.deepLink));
    profilesOrdinary.profilesOrdinary.map( m=> this.loadProfilesOrdinary(m.link, m.name, m.deepLink));
    sites.sites.filter(m =>m.enabled !== false).map( m=> this.loadSites(m.link, m.name));
    telegramChannels.telegramChannels.map( m=> this.loadTelegramChannels(m.link, m.name));
    twitterChannels.twitterChannels.map( m=> this.loadTwitterChannels(m.link, m.name));
    this.creator = "https://www.facebook.com/ivo.keeee";
    if(this.isMobile()){
      this.creator = "fb://profile/1067745810";
    }

  }

  public loadProfiles(link: string, name: string, deepLink: string){
    if(this.isMobile()){
      link = deepLink;
    }
    this.facebookFamous.push({link, name});
  }

  public loadProfilesOrdinary(link: string, name: string, deepLink: string){
    if(this.isMobile()){
      link = deepLink;
    }
    this.facebookOrdinary.push({link, name});
  }

  public loadSites(link: string, name: string){
    this.sites.push({link, name});
  }

  public loadYoutubeChannels(link: string, name: string){
    this.youtubeChannels.push({link, name});
  }

  public loadTelegramChannels(link: string, name: string){
    this.telegramChannels.push({link, name});
  }

  public loadTwitterChannels(link: string, name: string){
    this.twitterChannels.push({link, name});
  }

  public latestYoutubeClip(channelId: string, channelName: string){
    const cid = channelId;
    const channelURL = encodeURIComponent(`https://www.youtube.com/feeds/videos.xml?channel_id=${cid}`)
    const reqURL = `https://api.rss2json.com/v1/api.json?rss_url=${channelURL}&api_key=m4kvnbmenpvxt9asptp66bojesdu1yftfl223kvv`;

    this.http.get<any>(reqURL).subscribe(result => {
      const videoNumber = 0;
      const link = result.items[videoNumber].link;
      const id = link.substr(link.indexOf("=") + 1);
      this.youtubeChannels.push({link, channelName});
    })
  }
}
